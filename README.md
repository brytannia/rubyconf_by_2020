# Drawing with Ruby, OpenGL and GLFW

## Important links

1. Install [gem](https://github.com/vaiorabbit/ruby-opengl) `gem install opengl-bindings` 
1. How to build [GLFW](https://github.com/vaiorabbit/ruby-opengl/blob/master/sample/glfw_build.sh)
