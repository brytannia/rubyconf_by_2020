module Draw
  class Cone
    attr_reader :angle, :generatrix, :apex, :axis

    # angle [0..90]
    # generatrix [0..1]
    # apex [x,y,z]
    # axis [3..infinity]
    def initialize(angle, generatrix, apex, axis)
      @angle = angle * Math::PI / 180
      @generatrix = generatrix
      @apex = apex
      @axis = axis
    end

    def draw
      directrix_points = []
      increment_angle = 0
      (0..axis).each do |i|
        increment_angle += base_angle
        directrix_points.push(next_on_directrix(increment_angle))
      end

      glBegin(GL_TRIANGLES)
      directrix_points.each_with_index do |p, i|
        if i.even?
          Color.red
        else
          Color.pale_white
        end

        glVertex3f(*p)

        np = i+1 == directrix_points.length ? 0 : i+1

        glVertex3f(*directrix_points[np])

        if i.even?
          Color.dark_red
        else
          Color.white
        end

        glVertex3f(*apex)
      end

      glEnd()
    end

    private

    def base_angle
      @base_angle ||= 2 * Math::PI  / axis
    end

    def base_radius
      @base_radius ||= generatrix * Math.sin(angle)
    end

    def height
      @height ||= generatrix * Math.cos(angle)
    end

    def y_directrix
      @y_directrix ||= apex[1] - height
    end

    def x_directrix(angle)
      base_radius * Math.cos(angle)
    end

    def z_directrix(angle)
      base_radius * Math.sin(angle)
    end

    def next_on_directrix(angle)
      [x_directrix(angle), y_directrix, z_directrix(angle)]
    end
  end
end