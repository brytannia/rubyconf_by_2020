module Draw
  class Box
    attr_reader :width, :height

    def initialize(width, height)
      @width = width
      @height = height
    end

    def draw(centre)
      x_up = centre[0] + width/2.0
      x_down = centre[0] - width/2.0
      y_up = centre[1] + height/2.0
      y_down = centre[1] - height/2.0
      z_up = centre[2] + width/2.0
      z_down = centre[2] - width/2.0

      glBegin(GL_QUADS)
      #up
      Color.ghost_white
      glVertex3f(x_up, y_up, z_up)
      glVertex3f(x_down, y_up, z_up)
      Color.grey
      glVertex3f(x_down, y_up, z_down)
      glVertex3f(x_up, y_up, z_down)

      #down
      Color.ghost_white
      glVertex3f(x_up, y_down, z_up)
      glVertex3f(x_down, y_down, z_up)
      Color.grey
      glVertex3f(x_down, y_down, z_down)
      glVertex3f(x_up, y_down, z_down)

      #front
      Color.ghost_white
      glVertex3f(x_down, y_up, z_up)
      glVertex3f(x_up, y_up, z_up)
      Color.grey
      glVertex3f(x_up, y_down, z_up)
      glVertex3f(x_down, y_down, z_up)

      #back
      Color.ghost_white
      glVertex3f(x_down, y_up, z_down)
      glVertex3f(x_up, y_up, z_down)
      Color.grey
      glVertex3f(x_up, y_down, z_down)
      glVertex3f(x_down, y_down, z_down)

      # #left
      Color.ghost_white
      glVertex3f(x_down, y_down, z_up)
      glVertex3f(x_down, y_up, z_up)
      Color.grey
      glVertex3f(x_down, y_up, z_down)
      glVertex3f(x_down, y_down, z_down)

      #right
      Color.ghost_white
      glVertex3f(x_up, y_down, z_up)
      glVertex3f(x_up, y_up, z_up)
      Color.grey
      glVertex3f(x_up, y_up, z_down)
      glVertex3f(x_up, y_down, z_down)

      glEnd()
    end
  end
end