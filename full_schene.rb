# Based on the sample from https://github.com/vaiorabbit/ruby-opengl

# For more samples, visit https://github.com/vaiorabbit/ruby-opengl/tree/master/sample .
#
# Ref.: /glfw-3.0.1/examples/simple.c
#
require 'opengl'
require 'glfw'
require_relative 'color'
require_relative 'draw/box'
require_relative 'draw/cone'

OpenGL.load_lib()
GLFW.load_lib()

include OpenGL
include GLFW

# Press ESC to exit.
key_callback = GLFW::create_callback(:GLFWkeyfun) do |window_handle, key, scancode, action, mods|
  if key == GLFW_KEY_ESCAPE && action == GLFW_PRESS
    glfwSetWindowShouldClose(window_handle, 1)
  end
end

if __FILE__ == $0
  glfwInit()

  # Prepare window and controls
  window = glfwCreateWindow(640, 480, "Ruby ruby RUBY", nil, nil)
  glfwMakeContextCurrent(window)
  glfwSetKeyCallback(window, key_callback)

  # umbrella rotation
  angle = 10
  min_angle = 10
  max_angle = 80
  incr = 0.2

  # Main loop
  while glfwWindowShouldClose(window) == 0
    # Get current window size and use it to setup the viewport
    # _ptr because we need pointers
    width_ptr = ' ' * 8
    height_ptr = ' ' * 8
    glfwGetFramebufferSize(window, width_ptr, height_ptr)
    width = width_ptr.unpack('L')[0]
    height = height_ptr.unpack('L')[0]
    ratio = width.to_f / height.to_f
    glViewport(0, 0, width, height)

    # projection
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-ratio, ratio, -1.0, 1.0, 1.0, -1.0)

    # render all anew
    glClear(GL_COLOR_BUFFER_BIT)

    glLoadIdentity()

    # coordinates
    glBegin(GL_LINES)
    Color.red
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(1.0, 0.0, 0.0)
    Color.green
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 1.0, 0.0)
    Color.blue
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.0, 1.0)
    glEnd

    glLoadIdentity()

    #rotate umbrella
    glRotatef(glfwGetTime() * 50.0, 0.0, 1.0, 0.0)
    glBegin(GL_QUADS)

    Color.ghost_white
    Draw::Box.new(0.1, 0.1).draw([0.0, 0.0, 0.0])
    Draw::Box.new(0.02, 0.8).draw([0.0, 0.45, 0.0])

    angle += incr
    if angle < min_angle
      angle = min_angle
      incr = 0.2
    end

    if angle > max_angle
      angle = max_angle
      incr = -0.2
    end

    Draw::Cone.new(angle, 0.4, [0.0, 0.75, 0.0], 12).draw

    glfwSwapBuffers(window)
    glfwPollEvents()
  end

  glfwDestroyWindow(window)
  glfwTerminate()
end
