# Based on the sample from https://github.com/vaiorabbit/ruby-opengl

# For more samples, visit https://github.com/vaiorabbit/ruby-opengl/tree/master/sample .
#
# Ref.: /glfw-3.0.1/examples/simple.c
#
require 'opengl'
require 'glfw'

OpenGL.load_lib()
GLFW.load_lib()

include OpenGL
include GLFW

# Press ESC to exit.
key_callback = GLFW::create_callback(:GLFWkeyfun) do |window_handle, key, scancode, action, mods|
  if key == GLFW_KEY_ESCAPE && action == GLFW_PRESS
    glfwSetWindowShouldClose(window_handle, 1)
  end
end

if __FILE__ == $0
  glfwInit()

  # Prepare window and controls
  window = glfwCreateWindow(640, 480, "Ruby ruby RUBY", nil, nil)
  glfwMakeContextCurrent(window)
  glfwSetKeyCallback(window, key_callback)

  # Main loop
  while glfwWindowShouldClose(window) == 0
    # Get current window size and use it to setup the viewport
    # _ptr because we need pointers
    width_ptr = ' ' * 8
    height_ptr = ' ' * 8
    glfwGetFramebufferSize(window, width_ptr, height_ptr)
    width = width_ptr.unpack('L')[0]
    height = height_ptr.unpack('L')[0]
    ratio = width.to_f / height.to_f
    glViewport(0, 0, width, height)

    # projection
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-ratio, ratio, -1.0, 1.0, 1.0, -1.0)

    # render all anew
    glClear(GL_COLOR_BUFFER_BIT)

    #draw here

    glfwSwapBuffers(window)
    glfwPollEvents()
  end

  glfwDestroyWindow(window)
  glfwTerminate()
end
