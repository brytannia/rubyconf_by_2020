class Color
  class << self
    def red
      glColor3f(1.0, 0.0, 0.25)
    end

    def dark_red
      glColor3f(0.69, 0.0, 0.25)
    end

    def green
      glColor3f(0.0, 0.65, 0.0)
    end

    def blue
      glColor3f(0.0, 0.08, 1.0)
    end

    def yellow
      glColor3f(1.0, 0.91, 0.0)
    end

    def black
      glColor3f(0.0, 0.0, 0.0)
    end

    def white
      glColor3f(1.0, 1.0, 1.0)
    end

    def pale_white
      glColor3f(0.97, 0.98, 0.94)
    end

    def purple
      glColor3f(0.4, 0.2, 0.6)
    end

    def sky_blue
      glColor3f(0.52, 0.80, 0.92)
    end

    def ghost_white
      glColor3f(0.97, 0.97, 1.0)
    end

    def grey
      glColor3f(0.76, 0.69, 0.50)
    end
  end
end